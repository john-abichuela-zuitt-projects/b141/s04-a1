Activity

SELECT name FROM artists WHERE name LIKE "%d%";

SELECT * FROM songs WHERE length < 230;

SELECT albums.name AS album_name, title, length FROM songs JOIN albums 
ON albums.id = album_id; 

SELECT name AS album_name FROM albums JOIN songs ON albums.id = album_id
WHERE name LIKE '%A%';

SELECT * FROM albums ORDER BY name DESC LIMIT 4;

SELECT name AS album_name, title FROM
(SELECT @row := @row +1 AS row, name
  FROM albums, (SELECT @row := 0) rr
  ORDER BY name DESC
) album
INNER JOIN
(
  SELECT @row2 := @row2 + 1 AS row, title
  FROM songs, (SELECT @row2 := 0) rr
  ORDER BY title ASC
) song
ON album.row = song.row;

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY name DESC, songs.title ASC;
